const { reduce } = require('../problems/reduce');

const items = [1, 2, 3, 4, 5, 5];

const testCase3 = reduce(items, (accumulator, currentValue) => {
    return accumulator + currentValue;
}, 5);

console.log(testCase3);