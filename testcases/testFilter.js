
const { filter } = require('../problems/filter');

const items = [1, 2, 3, 4, 5, 5];

const testCase4 = filter(items, element => {
    return element > 3;
});

console.log(testCase4);