

function find(elements, cb) {
    if (cb === undefined || typeof cb !== 'function') {
        console.log('please provide a call back function');
        return
    }
    if (Array.isArray(elements)) {
        for (let index = 0; index < elements.length; index++) {
            if (cb(elements[index], index, elements)) {
                return elements[index]
            } else {
                return 'undefined.'
            }
        }
    } else {
        console.log('please provide an array');
    }
}


module.exports = { find };