

function reduce(elements, cb, startingValue) {
    if (cb === undefined || typeof cb !== 'function') {
        console.log('please provide a function.')
        return
    }

    if (Array.isArray(elements)) {
        let result = startingValue ? startingValue : elements[0]
        const startingIndex = startingValue ? 0 : 1;
        for (let index = startingIndex; index < elements.length; index++) {
            result = cb(result, elements[index])
        }
        return result
    }
}

module.exports = { reduce }