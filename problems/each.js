
// creating forEach function
// items.forEach((ele, index, array) => {
//     console.log(ele, index, array);
// })

function each(elements, cb) {
    if (cb === undefined || typeof cb !== 'function') {
        console.log('please provide a function');
        return
    }
    if (Array.isArray(elements)) {
        for (let index = 0; index < elements.length; index++) {
            cb(elements[index], index, elements)
        }
    } else {
        console.log(`please provide array`)
    }

}

module.exports = { each };


