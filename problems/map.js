// creating map function

/* array.map( x => {
    console.log (x*x)  
})*/


function map(elements, cb) {
    if (cb === undefined || typeof cb !== 'function') {
        console.log('please provide function');
        return 
    }

    if (Array.isArray(elements)) {
        const arr = [];
        for (let index = 0; index < elements.length; index++) {
            arr.push(cb(elements[index], index, elements))
        }
        return arr;
    } else {
        console.log('please provide an array');
    }
}

module.exports = { map };