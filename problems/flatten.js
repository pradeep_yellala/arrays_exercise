


function flatten(elements) {

    if (Array.isArray(elements)) {
        let arr = []
        for (item of elements) {
            if (Array.isArray(item)) {
                arr = arr.concat(flatten(item));
            } else {
                arr.push(item)
            }
        }
        return arr;
    } else {
        console.log('please provide an array');
    }
}


module.exports = { flatten }