

function filter(elements, cb) {
    if (cb === undefined || typeof cb !== 'function') {
        console.log('please provide a function');
        return
    }
    if (Array.isArray(elements)) {
        const arr = []
        for (let index = 0; index < elements.length; index++) {
            if (cb(elements[index], index, elements)) {
                arr.push(elements[index]);
            }
        }
        return arr
    } else {
        console.log('please provide an array');
    }
}


module.exports = { filter };